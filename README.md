![Logo](Diagrams/Meet2BreedLogo.PNG)

Erstellt in AndroidStudio unter Verwendung von Google Firebase.
Projekt von Nils Behrens, Ralf Herrmann, Karl Marggraff,  Killian Mauson, Johanna Meyer und Bennedict Schweimer.
Entstanden im Rahmen des Moduls "Software Engineering" an der HAW Hamburg.

## User Stories

1. Als User möchte ich einen Account erstellen können, um meine Hunde darauf speichern zu 
können.

2. Als User möchte ich mich in meinen Account einloggen können, um meine zuvor gespeicherten 
Hunde einsehen und matchen zu können.

3. Als User möchte ich meine bei Meet2Breed angemeldeten Hunde angezeigt bekommen, um 
diese mit anderen Hunden zu matchen.

4. Als User möchte ich neue Hunde hinzufügen können, um zu ihnen passende Hunde zur Züchtung 
zu finden.

5. Als User möchte ich einstellen können von welcher Rasse mir Hunde angezeigt werden, um 
Welpen einer bestimmten Rasse zu züchten.

6. Als User möchte ich einstellen können bis zu welchem Alter mir Hunde angezeigt werden, um 
Hunde in meinem Wunschalter anzeigen zu lassen.

7. Als User möchte ich eine Preisspanne einstellen können, um Hunde in meinem Budget angezeigt 
zu bekommen.

8. Als User möchte ich einstellen können, ob der andere Hund Papiere besitzen soll, damit die 
Welpen auch Papiere haben können.

9. Als User möchte ich nur Hunde angezeigt bekommen, die sich zur Züchtung mit meinem Hund 
eignen, um schneller passende Hunde zu finden.

10. Als User möchte ich Hunde, die mir gefallen, markieren können, um den anderen Besitzer zu 
benachrichtigen.

11. Als User möchte ich Bilder und Informationen der anderen Hunde sehen, um deren Eignung zur 
Züchtung mit meinem Hund besser einschätzen zu können.

12. Als User möchte ich sehen können, welche Hunde Interesse an meinem Hund haben, um 
schneller passende Hunde zu finden.

13. Als User möchte ich bei beidseitigem Interesse die Kontaktdaten des anderen Besitzers angezeigt 
bekommen, um Kontakt aufnehmen zu können.

14. Als User möchte ich bei beidseitigem Interesse die Kontaktdaten des anderen Besitzers auch zu 
einem späteren Zeitpunkt noch einsehen können, um Kontakt aufnehmen zu können.

15. Als User möchte ich die Daten meines Hundes verändern können, um ihn für andere User 
ansprechender erscheinen zu lassen.

16. Als User möchte ich meine eigenen Anmeldedaten aktualisieren können, damit andere 
Hundebesitzer mich kontaktieren können

## Reduziertes Klassendiagramm

![Klassendiagramm](Diagrams/KlassendiagrammM2B.PNG)

## Sequenzdiagramm Hund-Erstellung

![Sequenzdiagramm](Diagrams/SequenzM2B.PNG)
